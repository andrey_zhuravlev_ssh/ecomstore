# this stuff goes at the top of the file, below other imports
from django.core import urlresolvers

from django.http import HttpResponseRedirect
from cart import cartpy as cart

from django.shortcuts import get_object_or_404, render_to_response
from catalog.models import Category, Product
from django.template import RequestContext
from checkout import checkoutpy as checkout


# def show_cart(request, template_name="cart/cart.html"):
#     # cart_item_count = cart.cart_item_count(request)
#     cart_item_count = cart.cart_distinct_item_count(request)
#     print request
#     page_title = 'Shopping Cart'
#     return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def show_cart(request, template_name="cart/cart.html"):
    if request.method == 'POST':
        postdata = request.POST.copy()
        if postdata['submit'] == 'Remove':
            cart.remove_from_cart(request)
        if postdata['submit'] == 'Update':
            cart.update_cart(request)
        if postdata['submit'] == 'Checkout':
            # checkout_url = checkout.get_checkout_url(request)
            checkout_url = "/checkout"
            return HttpResponseRedirect(checkout_url)
    cart_items = cart.get_cart_items(request)
    page_title = 'Shopping Cart'
    cart_subtotal = cart.cart_subtotal(request)
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))