from django.http import HttpResponseRedirect
from cart.forms import ProductAddToCartForm
from cart import cartpy
from django.shortcuts import get_object_or_404, render_to_response
from catalog.models import Category, Product, ProductReview
from catalog.forms import ProductReviewForm
from django.template import RequestContext
from django.core import urlresolvers

from stats import statspy as stats
from ecomstore.settings import PRODUCTS_PER_ROW


from django.http import HttpResponse
from django.core import serializers

from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.template.loader import render_to_string
# from django.utils import simplejson
import json as simplejson
from django.http import HttpResponse
import tagging

from tagging.models import Tag, TaggedItem


def tag_cloud(request, template_name="catalog/tag_cloud.html"):
    product_tags = Tag.objects.cloud_for_model(Product, steps=9,distribution=tagging.utils.LOGARITHMIC,filters={ 'is_active': True })
    page_title = 'Product Tag Cloud'
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))


def tag(request, tag, template_name="catalog/tag.html"):
    products = TaggedItem.objects.get_by_model(Product.active, tag)
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

@login_required
def add_tag(request):
    print "dd tag"
    tags = request.POST.get('tag','')
    slug = request.POST.get('slug','')
    print  tags
    print  slug
    print  len(tags)
    if len(tags) > 2:
        p = Product.active.get(slug=slug)
        html = u''
        template = "catalog/tag_link.html"
        for tag in tags.split():
            tag.strip(',')
            Tag.objects.add_tag(p,tag)
        for tag in p.tags:
        # for tag in Tag.objects.all():
            html += render_to_string(template, {'tag': tag })
        print html
        response = simplejson.dumps({'success': 'True', 'html': html })
        print response
    else:
        response = simplejson.dumps({'success': 'False'})
    return HttpResponse(response, content_type='application/javascript; charset=utf8')

@login_required
@csrf_protect
def add_review(request):
    form = ProductReviewForm(request.POST)
    if form.is_valid():
        review = form.save(commit=False)
        slug = request.POST.get('slug')
        product = Product.active.get(slug=slug)
        review.user = request.user
        review.product = product
        review.save()
        template = "catalog/product_review.html"
        html = render_to_string(template, {'review': review })
        response = simplejson.dumps({'success':'True', 'html': html})
    else:
        html = form.errors.as_ul()
        response = simplejson.dumps({'success':'False', 'html': html})
    return HttpResponse(response, content_type='application/javascript; charset=utf-8')


def get_json_products(request):
    products = Product.active.all()
    json_products = serializers.serialize("json", products)
    return HttpResponse(json_products, content_type='application/javascript; charset=utf-8')


def index(request, template_name="catalog/index.html"):
    search_recs = stats.recommended_from_search(request)
    featured = Product.featured.all()[0:PRODUCTS_PER_ROW]
    recently_viewed = stats.get_recently_viewed(request)
    view_recs = stats.recommended_from_views(request)
    page_title = 'Musical Instruments and Sheet Music for Musicians'
    # return get_json_products(request)
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))


def show_category(request, category_slug, template_name="catalog/category.html"):
    c = get_object_or_404(Category, slug=category_slug)
    products = c.product_set.all()
    page_title = c.name
    meta_keywords = c.meta_keywords
    meta_description = c.meta_description

    return render_to_response(template_name, locals(), context_instance=RequestContext(request))


# def show_product(request, product_slug, template_name="catalog/product.html"):
#     p = get_object_or_404(Product, slug=product_slug)
#     categories = p.categories.filter(is_active=True)
#     page_title = p.name
#     meta_keywords = p.meta_keywords
#     meta_description = p.meta_description
#     return render_to_response(template_name, locals(),context_instance=RequestContext(request))

# new product view, with POST vs GET detection
def show_product(request, product_slug, template_name="catalog/product.html"):
    p = get_object_or_404(Product, slug=product_slug)
    categories = p.categories.all()
    page_title = p.name
    meta_keywords = p.meta_keywords
    meta_description = p.meta_description
    # need to evaluate the HTTP method
    if request.method == 'POST':
        # add to cart...create the bound form
        postdata = request.POST.copy()
        form = ProductAddToCartForm(request, postdata)
        # check if posted data is valid
        if form.is_valid():
            # add to cart and redirect to cart page
            cartpy.add_to_cart(request)
            # if test cookie worked, get rid of it
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
                url = urlresolvers.reverse('show_cart')
                return HttpResponseRedirect(url)
    else:
        form = ProductAddToCartForm(request=request, label_suffix=':')
        # assign the hidden input the product slug
    form.fields['product_slug'].widget.attrs['value'] = product_slug
        # set the test cookie on our first GET request
    request.session.set_test_cookie()

    from stats import statspy as stats
    stats.log_product_view(request, p) # add to product view

    product_reviews = ProductReview.approved.filter(product=p).order_by('-date')
    review_form = ProductReviewForm()

    return render_to_response("catalog/product.html", locals(),context_instance=RequestContext(request))