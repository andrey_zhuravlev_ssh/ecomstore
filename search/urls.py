from django.conf.urls import patterns, include, url

urlpatterns = patterns('search.views',
                       (r'^results/$', 'results', {'template_name': 'search/results.html'}, 'search_results'),
                       )
