from django.conf.urls import include, url, patterns
from ecomstore import settings

urlpatterns = patterns('checkout.views',
                       url(r'^$', 'show_checkout', {'template_name': 'checkout/checkout.html'}, 'checkout'),
                       url(r'^receipt/$', 'receipt', {'template_name': 'checkout/receipt.html'}, 'checkout_receipt'),
                       )

# from django.conf.urls import patterns, include, url


# urlpatterns = patterns('cart.views',
#                        url(r'^$', 'show_cart', { 'template_name': 'cart/cart.html'}, 'show_cart'),
#                        )
