# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template import Context
from django.shortcuts import render_to_response
from django.template import RequestContext


def catalog(request):
    my_context = Context({ 'site_name': 'Modern Musician' })
    # response_html = render_to_response('sample.html', my_context)
    return render_to_response('sample.html', my_context)


def file_not_found_404(request):
    page_title = 'Page Not Found'
    return render_to_response('404.html', locals(),context_instance=RequestContext(request))