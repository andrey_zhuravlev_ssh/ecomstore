"""ecomstore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# from django.conf.urls import include, url
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static

import settings

# from catalog import urls

# urlpatterns = [
#     url(r'^admin/', include(admin.site.urls)),
#     # url(r'^catalog/$', catalog),
#     # url(r'^catalog/$', 'preview.views.home'),
#     url(r'^', include('catalog.urls')),
#     (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
# ]

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'specstali.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('catalog.urls')),
    (r'^cart/', include('cart.urls')),
    (r'^checkout/', include('checkout.urls')),
    # (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^accounts/', include('accounts.urls')),
    (r'^accounts/', include('django.contrib.auth.urls')),
    (r'^search/', include('search.urls')),
)


# handler500 = 'views.file_not_found_404'
# handler400 = 'views.file_not_found_404'
# handler403 = 'views.file_not_found_404'
handler404 = 'ecomstore.views.file_not_found_404'

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


